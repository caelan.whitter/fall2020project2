package spellingbee.server;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;


import spellingbee.client.ISpellingBeeGame;
/**
 * 
 * @author Caelan
 *
 */
public class SpellingBeeGame implements ISpellingBeeGame {
	
	private String letters;
	private char center;
	private int currentScore;
	private List<String> found = new ArrayList<String>();
	private static List<String> possibleWords = createWordsFromFile("datafiles\\english.txt");
			
	/**
	 * 
	 * creates a new list, cycles through the text file and adds  
	 * it to possible words list	
	 * 	
	 * @param takes in a string representing a path
	 * @return a list containing possible words from txt file
	 */
	public static List<String> createWordsFromFile (String path)
	{
		List<String> possible2 = new ArrayList<String>();
		try {
		
		Scanner s = new Scanner(new File(path));
		while(s.hasNext())
		{
			possible2.add(s.next());
		}
		s.close();
		}
		catch(FileNotFoundException ex){
			System.out.println(ex);
		}
		return possible2;
		
		
	}
	/**
	 * Creates a new list and adds every seven letter combo to it
	 * 
	 *Chooses a random seven letter combo to display
	 *
	 *Chooses a random center letter from that seven letter combo 
	 */
	public SpellingBeeGame()  
	{
		try
		{
		List<String> sevletters = new ArrayList<String>();
		File file = new File("datafiles\\letterCombinations.txt");
		Scanner s = new Scanner(file);
		while(s.hasNext())
		{
			
			sevletters.add(s.next());
		}
		s.close();
		
		Random random = new Random();
		int randNum = random.nextInt(sevletters.size());
		this.letters=sevletters.get(randNum);
		
		
		int randNum2 = random.nextInt(this.letters.length());
		this.center = this.letters.charAt(randNum2);

		this.currentScore = 0;
		
		}
		catch(FileNotFoundException e)
		{
			System.out.println(e);
		}
		
		
		
	}
	/**
	 * Takes in a parameter for the random seven letters and chooses a random center letter from it
	 * @param givenLetters
	 */
	public SpellingBeeGame(String givenLetters)
	{

		
		Random random = new Random();

		int randNum = random.nextInt(givenLetters.length()+1);
		this.center = this.letters.charAt(randNum);

		this.currentScore = 0;
		this.found=null;
	}
	
	/**
	 * takes in a word
	 * if the word does not contain the 7 letters return 0
	 * if the word does not exist return 0
	 * if the word does not contain center letter return 0
	 * if the word is already found return 0
	 * if the word is of length four return 1
	 * else return length of word
	 * if panagram return length plus 7
	 * @param attempt
	 */
	@Override
	public int getPointsForWord(String attempt)
	{
		int points = 0;
		int points2 = 7;
		

		if(!containing(attempt))
		{
			return 0;
		}
		else if(!possibleWords.contains(attempt))
		{
			return 0;
		}
		else if(!attempt.contains(Character.toString(this.center)) || attempt.length()<4)
		{

			return 0;
		}
		else if(found.contains(attempt))
		{

			return 0;
		}
		else if(attempt.length() == 4)
		{
			points=1;
		}
		else
		{
			
			points=attempt.length();
		}
		
		
	
		if(allSeven(attempt)==7)
		{
			points = points+points2;
		}
		
		found.add(attempt);
		this.currentScore= this.currentScore+points;
		return points;
		
	}
	
	/**
	 * checks if the word contains all 7 letters 
	 * @param attempt
	 * @return
	 */
	public int allSeven(String attempt)
	{
		int points2 = 0;
		for (char c : this.letters.toCharArray())
		{
			if (attempt.indexOf(c) != -1)
			{
				points2++;
			}
		}
		return points2;
	}
	
	/**
	 * checks if the attempt contains only the seven letters given
	 * @param attempt
	 * @return boolean
	 */
	public boolean containing(String attempt)
	{
		for (char c : attempt.toCharArray())
		{
			if (this.letters.indexOf(c) == -1)
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * if attempt shorter than 4 returns word too short
	 * if doesnt contain center letter return doesnt contain center
	 * if doesnt contain only the seven letters returns doesnt contain
	 * if word is not in possible words returns doesnt exist
	 * if word in found return already used
	 * else return good
	 * @return string
	 */
	@Override
	public String getMessage(String attempt) 
	{
		String message;
		
		if(attempt.length()<4)
		{
			message = "The word is too short";
		}
		else if (!attempt.contains(Character.toString(this.center)))
		{
			message = "Word does not contain center letter";
		}
		else if (!containing(attempt))
		{
			message = "Word contains a letter other than the 7 presented";
		}
		else if (!possibleWords.contains(attempt))
		{
			message = "Word does not exist";
		}
		else if(found.contains(attempt))
		{
			message = "Word already used";
		}
		else
		{
			
			message = "good!";
			
		}
		

		return message;
	}

	/**
	 * returns all seven letters
	 * @return String
	 */
	@Override
	public String getAllLetters()
	{
		
		return this.letters;
	}

	/**
	 * returns center letter
	 * @return Char
	 */
	@Override
	public char getCenterLetter() 
	{
		return this.center;
	}

	/**
	 * returns current score
	 * @return int
	 */
	@Override
	public int getScore() 
	{
		return this.currentScore;
	}

	/**
	 * 
	 * goes through possible words
	 * if possiblewords contains attempt continue
	 * if word contains center letter and bigger than 4 continue
	 * adds score to maximum depending on length
	 * 
	 * adds to int [] different percentages of maximum score
	 * 
	 * 
	 * @return int[]
	 */
	@Override
	public int[] getBrackets() 
	{
		

		int maximum=0;
		for(String word : possibleWords)
		{

			if(containing(word))
			{
				
				if(word.contains(Character.toString(this.center)) && word.length()>=4)
				{
					System.out.println(word);

					if(word.length() == 4)
					{
						maximum = maximum +1;
					}
					else
					{
					
						maximum =maximum + word.length();
					}
				
				
					if(allSeven(word)==7)
					{
						maximum = maximum+7;
					}
				
				}
				else 
				{
					maximum = maximum+0;
				}
			
			}
		}
		
		int[] nums = new int[5];
		nums[4] = (int) (maximum*0.25);
		nums[3] = (int) (maximum*0.50);
		nums[2] = (int) (maximum*0.75);
		nums[1] = (int) (maximum*0.90);
		nums[0] = maximum;
		

		return nums;
	}

}
