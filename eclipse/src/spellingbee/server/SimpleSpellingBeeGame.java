package spellingbee.server;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import spellingbee.client.ISpellingBeeGame;

/**
 * @author Mikael
 */
public class SimpleSpellingBeeGame implements ISpellingBeeGame {
	
	private String word;
	private char centerLetter;
	private int currentScore;
	private int points;
	private List<String> found = new ArrayList<String>();
	String message;
	
	//Constructor for hardcoded values
	public SimpleSpellingBeeGame() {
	
		this.word = "acdemis";
		this.centerLetter = 'e';
		this.currentScore=0;
		this.points=0;
	}
	
	
	@Override
	//Getting points according to rules of game
	/**
	 * @param attempt the String inputed
	 * @return points received according to input
	 */
	public int getPointsForWord(String attempt) {
		
		if(!attempt.contains(Character.toString(this.centerLetter)))
		{
			return 0;
		}
		
		else if(attempt.length()<4)
		{
			return 0;
		}
		
		else if(getMessage(attempt)!="good!"&&getMessage(attempt)!="It's a panagram!") 
		{
			return 0;
		}
		
		else if(allSeven(attempt)==7)
		{	
			points = attempt.length()+7;
		}
		
		else if(attempt.length()==4)
		{
			points = 1;
		}
		
		else 
		{	
			points=attempt.length();	
		}
		
		found.add(attempt);
		this.currentScore = this.currentScore+points;
		return points;
	}
		//Getting message for every condition according to game rules
		/**
		 * @param attempt the String inputed
		 * @return message displayed according to input
		 */
		public String getMessage(String attempt) 
		{
			File file = new File("datafiles\\english.txt");
		
			Scanner scan;
			List<String> wordFromFile = new ArrayList<String>();
			try 
			{
				scan = new Scanner(file);
				while(scan.hasNext()) {
					wordFromFile.add(scan.next());
					
				}
				scan.close();
				
			} 
			catch (FileNotFoundException e) 
			{
				System.out.println(e);
				e.printStackTrace();
			}
			
			if(attempt.length()<4)
			{
				System.out.println(attempt);
				message = "The word is too short";
			}
			
			else if (!attempt.contains(Character.toString(this.centerLetter)))
			{
				message = "Word does not contain center letter";
			}
			
			else if (!wordFromFile.contains(attempt))
			{
				message = "Word does not exist";
			}
			
			else if (!containing(attempt))
			{
				message = "Word contains a letter other than the 7 presented";
			}
			
			else if(found.contains(attempt))
			{
				message = "Word already used";
			}
			
			else if (allSeven(attempt)==7)
			{
				message = "It's a panagram!";
			}
			
			else
			{	
				message = "good!";	
			}
		
			return message;
		}

	// Get method to receive hardcoded letters in word
	public String getAllLetters() 
	{
		return this.word;
	}
	//Get method to receive the chosen hardcoded center letter
	public char getCenterLetter() 
	{	
		return this.centerLetter;
	}
	// Get method to receive the current accumulated score from start of game
	public int getScore() 
	{	
		return this.currentScore;
	}

	@Override
	public int[] getBrackets() {
		
		int[] ranking = new int[5];
		ranking[4] = 50;
		ranking[3] = 100;
		ranking[2] = 150;
		ranking[1] = 180;
		ranking[0] = 200;

		return ranking;
	}
		// Created method shared with Caelan (check if all seven letters are in inputed word)
		/**
		 * @param attempt the String inputed
		 * @return panagram (points returned with added 7 points)
		 */
		public int allSeven(String attempt)
		{
			int panagram = 0;
			for (char c : this.word.toCharArray())
			{
				if (attempt.indexOf(c) != -1)
				{
					panagram++;
				}
			}
			return panagram;
		}
		
		// Created method shared with Caelan (check if there are letters inputed that are not from the hardcoded values)
		/**
		 * @param attempt The String inputed
		 * @return true or false Depending if @word has all chars from attempt 
		 */
		public boolean containing(String attempt)
		{
			for (char c : attempt.toCharArray())
			{
				if (this.word.indexOf(c) == -1)
				{
					return false;
				}
			}
			return true;
		}

	 
		
}
