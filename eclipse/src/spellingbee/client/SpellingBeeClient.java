package spellingbee.client;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import spellingbee.network.Client;
import spellingbee.network.HighScores;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
/**
 * @author Caelan
 * @author Mikael
 * @author Momshad
 *
 */
public class SpellingBeeClient extends Application{
	private Client client;

	public void start(Stage stage) throws Exception {
		this.client = new Client();
	    TabPane tapPane = new TabPane();


	     Tab t1 = new GameTab(client);
	     Tab t2 = new ScoreTab(client,((GameTab) t1).getScoreField());
	     Tab t3 = new HighScores(client,((GameTab) t1).getScoreField());



	     Scene scene = new Scene(tapPane,700,400);


		tapPane.getTabs().add(t1);
		tapPane.getTabs().add(t2);
		tapPane.getTabs().add(t3);


		

	    stage.setScene(scene);

	    stage.show();

		

	}
    public static void main(String[] args) {
        Application.launch(args);
    }
}
