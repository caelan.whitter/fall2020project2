package spellingbee.client;
/**
 * @author Caelan
 * @author Mikael
 * @author Momshad
 *
 */
public interface ISpellingBeeGame {
	/* This message should return the number of points
	that a given word is worth according to the
	 SpellingBee rules.
	*/
	int getPointsForWord(String attempt);
	
	/*This method should check if the word attempt is a 
	 * valid word or not according to the Spelling Bee 
	 * rules. It should return a message based on the 
	 * reason it is rejected or a positive message
	 *  (e.g. �good� or �great�) if it is a valid word.
	 */
	String getMessage(String attempt);
	
	/*
	 * This method should return the set of 7 letters 
	 * (as a String) 
	 * that the spelling bee object is storing
	 */
	String getAllLetters();
	
	/*
	 * This method should return the center character. 
	 * That is, the character that is required to be 
	 * part of every word.
	 */
	char getCenterLetter();
	
	/*
	 * This method should return the current score of 
	 * the user.
	 */
	int getScore();
	
	/*
	 * This method will be used in the gui to determine
	 *  the various point categories.
	 */
	int[] getBrackets();

}
