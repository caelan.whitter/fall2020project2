
/**
 * 
 * @author Mikael Baril
 * 
 */
package spellingbee.client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import spellingbee.network.Client;

public class GameTab extends Tab{

	private TextField numResult;
    private Client client;
    

    //Constructor 
    /**
     * 
     * @param client
     */


	public GameTab(Client client)
    {
        super("GameTab");
        this.client=client;
        Group root = new Group(); 
    



        //buttons
        HBox buttons = new HBox();

        String response = client.sendAndWaitMessage("getCenter");
        String response2 = client.sendAndWaitMessage("getLetters");
        String one = Character.toString(response2.charAt(0));
        String two = Character.toString(response2.charAt(1));
        String three = Character.toString(response2.charAt(2));
        String four = Character.toString(response2.charAt(3));
        String five = Character.toString(response2.charAt(4));
        String six = Character.toString(response2.charAt(5));
        String seven = Character.toString(response2.charAt(6));

        Button letter1 = new Button(one);
        Button letter2 = new Button(two);
        Button letter3 = new Button(three);
        Button letter4 = new Button(four);
        Button letter5 = new Button(five);
        Button letter6 = new Button(six);
        Button letter7 = new Button(seven);
        
        if(response.equals(one))
        {
            letter1.setTextFill(Color.RED);

        }
        if(response.equals(two))
        {
            letter2.setTextFill(Color.RED);

        }
        if(response.equals(three))
        {
            letter3.setTextFill(Color.RED);

        }
        if(response.equals(four))
        {
            letter4.setTextFill(Color.RED);

        }
        if(response.equals(five))
        {
            letter5.setTextFill(Color.RED);

        }
        if(response.equals(six))
        {
            letter6.setTextFill(Color.RED);

        }
        if(response.equals(seven))
        {
            letter7.setTextFill(Color.RED);

        }



        buttons.getChildren().addAll(letter1,letter2,letter3,letter4,letter5,letter6,letter7);

        

       
       
        
        //submit
        Button submit = new Button("Submit");

        //results
        HBox result = new HBox();

        TextField wordResult = new TextField();

         numResult = new TextField();


      //word
        TextField word = new TextField();
        	
        // Clear
        Button clear = new Button("Clear");
        
        clear.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                word.setText("");
            } 
       }
       );
        
        letter1.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                word.appendText(one);
            } 
       }
       );
        letter2.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                word.appendText(two);
            } 
       }
       );
        letter3.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                word.appendText(three);
            } 
       }
       );
        letter1.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                word.appendText(one);
            } 
       }
       );
        letter4.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                word.appendText(four);
            } 
       }
       );
        letter5.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                word.appendText(five);
            } 
       }
       );
        letter6.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                word.appendText(six);
            } 
       }
       );
        letter7.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                word.appendText(seven);
            } 
       }
       );
        
        submit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                String response = client.sendAndWaitMessage("Try;"+word.getText());
                String[] messP = response.split(";");
                wordResult.setText(messP[0]);
                numResult.setText(messP[1]);
            } 
       }
       );
        
        result.getChildren().addAll(wordResult,numResult);

        //vbox
        VBox everything = new VBox();
        everything.getChildren().addAll(buttons,word,submit,clear,result);


        root.getChildren().add(everything);


        this.setContent(root);

    }
	// Getter to get the score
    public TextField getScoreField()
    {
    	return this.numResult;

    }


}
