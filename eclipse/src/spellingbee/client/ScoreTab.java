package spellingbee.client;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import spellingbee.network.Client;

/**
 * 
 * @author Caelan
 *
 */
public class ScoreTab extends Tab{
	
	private Client client;

	private Text score;
	private Text hundred;
	private Text ninety;
	private Text seventyFive;
	private Text fifty;
	private Text twentyFive;

	private Text queenBee;
	private Text genius;
	private Text amazing;
	private Text good;
	private Text gettingStarted;


	/**
	 * sets title to Score
	 * creates Gridpane
	 * Adds text for different achievements and text for percentages
	 * 
	 * whenever the Text containing the score it changed, call refresh()
	 * 
	 * sends message to server to get back to numbers to put in percentage spots
	 * 
	 * 
	 * @param client
	 * @param numUpdate
	 */
	public ScoreTab(Client client,TextField numUpdate)
	{
		super("Score");
		this.client=client;
	    GridPane gridpane = new GridPane();
	    
	    gridpane.setHgap(10);
	    
	    this.queenBee = new Text("Queen Bee");
	    this.genius = new Text("Genius");
	    this.amazing = new Text("Amazing");
	    this.good = new Text("Good");
	    this.gettingStarted = new Text("Getting Started");
	    Text currentScore = new Text("Current Score");
	    
	    this.hundred = new Text("100%");
	    this.ninety = new Text("90%");
	    this.seventyFive = new Text("75%");
	    this.fifty = new Text("50%");
	    this.twentyFive = new Text("25%");
	    this.score = new Text(numUpdate.getText());
	     
	    
	     
	    numUpdate.textProperty().addListener(new ChangeListener<String>() {
	        public void changed(ObservableValue<? extends String> observable,
	                String oldValue, String newValue) {

	            refresh();
	        }
	    });
	     
	     
	     
	    
		String response2 = client.sendAndWaitMessage("getBrackets");
		String[] pieces = response2.split(";");
		hundred.setText(pieces[0]);
		ninety.setText(pieces[1]);
		seventyFive.setText(pieces[2]);
		fifty.setText(pieces[3]);
		twentyFive.setText(pieces[4]);
		


	    
	    queenBee.setFill(Color.GREY);
	    genius.setFill(Color.GREY);
	    amazing.setFill(Color.GREY);
	    good.setFill(Color.GREY);
	    gettingStarted.setFill(Color.GREY);
	    
	    score.setFill(Color.RED);


	    
	    gridpane.add(queenBee, 0, 0);
	    gridpane.add(genius, 0, 1);
	    gridpane.add(amazing, 0, 2);
	    gridpane.add(good, 0, 3);
	    gridpane.add(gettingStarted, 0, 4);
	    gridpane.add(currentScore, 0, 5);
	    
	    gridpane.add(hundred, 1, 0);
	    gridpane.add(ninety, 1, 1);
	    gridpane.add(seventyFive, 1, 2);
	    gridpane.add(fifty, 1, 3);
	    gridpane.add(twentyFive, 1, 4);
	    gridpane.add(score, 1, 5);
		this.setContent(gridpane);
	}

	   

	    

	
	/**
	 * sends message to server to get score 
	 * 
	 * depending on the score, it'll either change the achievements to black or stay grey
	 * 
	 * sets the text containing current score to updated score
	 * 
	 * 
	 */
	public void refresh()
	{
		String response1 = client.sendAndWaitMessage("getScore");
		int response2 = Integer.parseInt(response1);

		if(response2 >= Integer.parseInt((this.twentyFive.getText())))
	    {
		    this.gettingStarted.setFill(Color.BLACK);

	    }
	    if(response2 >= Integer.parseInt((this.fifty.getText())))
	    {
		    this.good.setFill(Color.BLACK);

	    }
	    if(response2 >= Integer.parseInt((this.seventyFive.getText())))
	    {
		    this.amazing.setFill(Color.BLACK);

	    }
	    if(response2 >= Integer.parseInt((this.ninety.getText())))
	    {
		    this.genius.setFill(Color.BLACK);

	    }
	    if(response2 >= Integer.parseInt((this.hundred.getText())))
	    {
		    this.queenBee.setFill(Color.BLACK);

	    }
	  
		this.score.setText(response1);

		

	
	}
		

}
