package spellingbee.network;

import javafx.event.ActionEvent;

/**
 * @author Momshad
 *
 */
public class Score implements Comparable<Score> {
	private int score;
	private String userName;
	
	public Score() {
		this.score = 0;
		this.userName = "";
	}
	
	/**
	 * default const.
	 * @param score
	 * @param username
	 */
	public Score(int score, String username) {
		this.score = score;
		this.userName = username;
	}
	
	
	/**
	 * Overriding toString method
	 */
	@Override
	public String toString() {
		return this.userName + ';' + this.score;
	}
	
	
	/**
	 * sorts the array list based on user score
	 */
	public int compareTo(Score scoreToCompareAgainst) {
		if(this.score > scoreToCompareAgainst.score) {
			return 1;
		}
		else if(this.score < scoreToCompareAgainst.score) {
			return -1;
		}
		else {
			return 0;
		}
	}
}
