	package spellingbee.network;
	
	import javafx.beans.value.ChangeListener;
	import javafx.beans.value.ObservableValue;
	import javafx.event.ActionEvent;
	import javafx.event.EventHandler;
	
	import java.io.BufferedWriter;
	import java.io.File;
	import java.io.FileNotFoundException;
	import java.io.FileWriter;
	import java.io.IOException;
	import java.util.ArrayList;
	import java.util.Collections;
	import java.util.Scanner;

import javax.swing.JPanel;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
	import javafx.scene.control.Tab;
	import javafx.scene.control.TextArea;
	import javafx.scene.control.TextField;
	import javafx.scene.layout.GridPane;
	import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import spellingbee.client.GameTab;
		
	/**
	 * @author Momshad
	 *
	 */
	public class HighScores extends Tab{
		private Client client;
		private int runningScore = 0;
		public ArrayList<Score> scores;

		
		
		
		/**
		 *setting scores to empty array list
		 */
		
		public HighScores() {
			this.scores = new ArrayList<Score>();
		}
		
		
		
		/**
		 * sets title to High Score
		 * creates Group
		 * creating buttons and textfield
		 * adding event listener to swap scores and saving it 
		 * whenever button save and refresh are clicked protocol is called
		 * top ten button call the protocol and returns the top 10
		 * creating a panel so when button is clicked top 10 results show
		 * 
		 * @param client
		 * @param numUpdate
		 */
		
		public HighScores(Client client,TextField numUpdate)
		{

			super("High Scores");
			this.client=client;
						
			Group group = new Group();
			
			TextField tf = new TextField("Enter User Name : ");

	        //TextArea textArea = new TextArea();
	        VBox vbox = new VBox();
		    Button save = new Button("Save");
		    Button refresh = new Button("Refresh");
		    Button topTen = new Button("Top 10 Scores");
		    
		    
		    vbox.getChildren().addAll(tf,save,refresh,topTen);
		    
		    group.getChildren().add(vbox);
		    this.setContent(group);
		    
		    
		    
		    numUpdate.textProperty().addListener(new ChangeListener<String>() {
		        public void changed(ObservableValue<? extends String> observable,
		                String oldScore, String newScore) {

		            runningScore = Integer.parseInt(newScore);
		        }
		    });
		    
		    refresh.setOnAction(new EventHandler<ActionEvent>() {
	            public void handle(ActionEvent e) {
	                tf.setText("Enter User Name : ");
	            } 
	       }
	       );
		    
		    topTen.setOnAction(new EventHandler<ActionEvent>() {
	            public void handle(ActionEvent e) {
	            	String response = client.sendAndWaitMessage("getTopTen");

	            	String[] responseS = response.split(",");
	            	Stage stage = new Stage();
	   			 stage.setTitle("Top 10 Scores");
	   			TextArea textArea = new TextArea();


	   			for(String s: responseS)
	   			{
		   			textArea.appendText(s+'\n');
		   			

	   			}

	   	        VBox vbox = new VBox(textArea);

	   	        Scene scene = new Scene(vbox, 400, 200);
	   	        stage.setScene(scene);
	   	        stage.show();
	            } 
	       }
	       );
		    
		    save.setOnAction(new EventHandler<ActionEvent>() {
	            public void handle(ActionEvent e) {
	         	client.sendAndWaitMessage(
	         				"saveAction" + ";" +
     						tf.getText() + ";"  +
     						runningScore
     						);

			       

			}
	            
	       }
	       );
		}
				
	//A constructor that takes as input a String representing a path to a file. 
	//The file format should be such that on each line is one result. 	
	public HighScores (String path) {
	this.scores = new ArrayList<Score>();
	readData(path);
	}
  
	
	private void readData(String path) {
	    try {
	        File myObj = new File(path);
	        Scanner myReader = new Scanner(myObj);
	        while (myReader.hasNextLine()) {
	          String individualScore = myReader.nextLine();
	          String [] gamerProperties = individualScore.split(";");
	          String playerName = gamerProperties[0];
	          int playerScore = Integer.parseInt(gamerProperties[1]);
	          Score obj = new Score(playerScore,playerName);
	          scores.add(obj);
	        }
	        myReader.close();
	      } catch (FileNotFoundException e) {
	        System.out.println("An error occurred.");
	        e.printStackTrace();
	      }
	     Collections.sort(scores);
	}
	
	
	
	/**
	 * adds a new result into scores array list
	 * sort array
	 * @param score
	 * @param name
	 */
	public void addResult(int score, String name) {
		scores.add(new Score(score, name));
		Collections.sort(scores);
	}
	
	/**
	 * checks to see if file exists or not
	 * then calls the scoreTostring method to place inside score.txt
	 * @param filepath
	 */
	public void saveResult(String filepath) throws IOException {
		BufferedWriter out = null;
		try {
		    FileWriter fstream = new FileWriter(filepath, false);	
		    out = new BufferedWriter(fstream);
		    out.write(this.scoreToString());
		}

		catch (IOException e) {
		    System.out.println("File doesn't exits!");
		}

		finally {
		    if(out != null) {
		        out.close();
		    }
		}
	}
	
	
	/**
	 * call scoreToString and passes to to print and sort top 10 scores
	 */
	public String getTopTen() {
		Collections.reverse(scores);
		return this.scoreToString(10);
	}
	
	/**
	 * prints and sorts the arraylist
	 */
	private String scoreToString() {
		String scoreData = "";
		for(int i=0; i<this.scores.size(); i++) {
			scoreData += this.scores.get(i).toString() + '\n';
		}
		
		return scoreData;
	}
	
	
	/**
	 * iterates on the size of the array and maximum 10 and prints writes to the file
	 */
	private String scoreToString(int maxElements) {

		String scoreData = "";
		for(int i=0; i < maxElements; i++) {
			scoreData += this.scores.get(i).toString() + ",";

		}
		
		return scoreData;
	}
}
