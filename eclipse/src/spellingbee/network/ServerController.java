package spellingbee.network;

import java.io.IOException;

import spellingbee.client.ISpellingBeeGame;
import spellingbee.server.*;

/**
 * This class will run on the server side and is used to connect the server code to the backend business code.
 * This class is where the "protocol" will be defined.
 */
/**
 * 
 * @author Caelan Whitter
 * @author Momshad Hussain
 * @author Mikael Baril
 *
 */
public class ServerController {
	// This is the interface you will be creating!
	// Since we are storing the object as an interface, you can use any type of ISpellingBeeGame object.
	// This means you can work with either the SpellingBeeGame OR SimpleSpellingBeeGame objects and can
	// seamlessly change between the two.
	private ISpellingBeeGame spellingBee = new SpellingBeeGame();

	

	final String path = "./datafiles/scores.txt";
	private HighScores hs = new HighScores(path);


	/**
	 * Action is the method where the protocol translation takes place.
	 * This method is called every single time that the client sends a request to the server.
	 * It takes as input a String which is the request coming from the client. 
	 * It then does some actions on the server (using the ISpellingBeeGame object)
	 * and returns a String representing the message to send to the client
	 * @param inputLine The String from the client
	 * @return The String to return to the client
	 * @throws IOException 
	 */
	public String action(String inputLine) throws IOException {

		/* Your code goes here!!!!
		 * Note: If you want to preserve information between method calls, then you MUST
		 * store it into private fields.
		 * You should use the spellingBee object to make various calls and here is where your communication
		 * code/protocol should go.
		 * For example, based on the samples in the assignment:
		 * if (inputLine.equals("getCenter")) {
		 * 	// client has requested getCenter. Call the getCenter method which returns a String of 7 letters
		 *      return spellingBee.getCenter();
		 * }
		 * else if ( ..... )*/
		
		if(inputLine.equals("getScore"))
		{
			String result = Integer.toString(this.spellingBee.getScore());
			return result;
		}
		else if(inputLine.equals("getBrackets"))
		{
			String result2 ="";
			
			for(int i:this.spellingBee.getBrackets())
			{
				result2 = result2 + Integer.toString(i)+";";
			}
			return result2;
		}
		else if(inputLine.equals("getLetters"))
		{
			String result3 = this.spellingBee.getAllLetters();
			return result3;
		}
		else if(inputLine.equals("getCenter")) {
			String result4 = Character.toString(this.spellingBee.getCenterLetter());
			return result4;
			
		}
		else if(inputLine.startsWith("Try"))
		{
			String[] result5 = inputLine.split(";");
			String message = this.spellingBee.getMessage(result5[1]);
			String points = String.valueOf(this.spellingBee.getPointsForWord(result5[1]));
			return message+";"+points;
		}
		else if(inputLine.equals("getTopTen"))
		{

			String result7 = (this.hs.getTopTen());
	
			return result7;

		}
		else if(inputLine.startsWith("saveAction"))
		{
			String[] result6 = inputLine.split(";");
			int score = Integer.parseInt(result6[2]);
			String name = result6[1];
			
           	this.hs.addResult(score, name);
       
			
			
            try {
            	this.hs.saveResult(this.path);
            	
			} catch (IOException e1) {
				e1.printStackTrace();
			}
            return score+";"+name;

		}
		else 
		{
		return null;
		}

	}


  }

